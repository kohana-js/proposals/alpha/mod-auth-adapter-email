const { KohanaJS } = require('kohanajs');
KohanaJS.initConfig(new Map([
  ['auth', require('./config/auth')],
  ['signup', require('./config/signup')],
  ['edm', require('./config/edm')],
]));

const { HTTP_METHODS } = require('@kohanajs/constants');
const { RouteList } = require('@kohanajs/mod-route');

RouteList.add('/signup', 'controller/Auth', 'signup_post', HTTP_METHODS.POST);

RouteList.add('/login', 'controller/Auth', 'login');
RouteList.add('/login', 'controller/Auth', 'auth', HTTP_METHODS.POST);
RouteList.add('/login/fail', 'controller/Auth', 'fail');
RouteList.add('/logout', 'controller/Auth', 'logout');

RouteList.add('/forgot-password', 'controller/Auth', 'forgot_password');
RouteList.add('/forgot-password', 'controller/Auth', 'forgot_password_post', HTTP_METHODS.POST);
RouteList.add('/forgot-password-submit', 'controller/Auth', 'forgot_password_result');
RouteList.add('/reset-password/:sign/:code', 'controller/Auth', 'reset_password');
RouteList.add('/reset-password/:sign/:code', 'controller/Auth', 'reset_password_post', HTTP_METHODS.POST);
RouteList.add('/reset-password-submit', 'controller/Auth', 'reset_password_result');
RouteList.add('/forgot-username', 'controller/Auth', 'forgot_username');
RouteList.add('/forgot-username', 'controller/Auth', 'forgot_username_post', HTTP_METHODS.POST);
RouteList.add('/forgot-username-submit', 'controller/Auth', 'forgot_username_result');

RouteList.add('/account/resend-verification', 'controller/Account', 'resend_verification');
RouteList.add('/account/change-email', 'controller/Account', 'change_email');
RouteList.add('/account/change-email', 'controller/Account', 'change_email_post', HTTP_METHODS.POST);

RouteList.add('/account/change-password', 'controller/Account', 'change_password');
RouteList.add('/account/change-password', 'controller/Account', 'change_password_post', HTTP_METHODS.POST);
RouteList.add('/account/change-password-submit', 'controller/Account', 'change_password_result');
RouteList.add('/account/activate/:code', 'controller/Account', 'activate');

RouteList.add('/account', 'controller/Account', 'index');

const fs = require('fs');
const path = require('path');
const Database = require('better-sqlite3');

const { ORM, KohanaJS } = require('kohanajs');
const { ORMAdapterSQLite } = require('@kohanajs/mod-database');

ORM.defaultAdapter = ORMAdapterSQLite;

KohanaJS.init({
  EXE_PATH: `${__dirname}/loginTest/test`,
  APP_PATH: `${__dirname}/loginTest/test`,
});

KohanaJS.initConfig(new Map([
  ['cookie', ''],
  ['session', ''],
  ['auth', ''],
  ['signup', ''],
  ['edm', ''],
]));

require('@kohanajs/mod-crypto');
require('@kohanajs/mod-session');
KohanaJS.classPath.set('helper/Registrar.js', require('../classes/helper/Registrar'));
KohanaJS.classPath.set('model/PasswordIdentifier.js', require('../classes/model/PasswordIdentifier'));
KohanaJS.classPath.set('model/Person.js', require('../classes/model/Person'));
KohanaJS.classPath.set('model/Role.js', require('../classes/model/Role'));
KohanaJS.classPath.set('model/User.js', require('../classes/model/User'));
KohanaJS.classPath.set('model/Login.js', require('../classes/model/Login'));
KohanaJS.classPath.set('Auth.js', require('../classes/Auth'));
KohanaJS.classPath.set('controller-mixin/Auth.js', require('../classes/controller-mixin/Auth'));
KohanaJS.classPath.set('controller-mixin/EmailRegister.js', require('../classes/controller-mixin/EmailRegister'));

const HelperRegistrar = require('../classes/helper/Registrar');
const ControllerAuth = require('../classes/controller/Auth');

describe('login test', () => {
  // copy db
  const target = path.normalize(`${__dirname}/loginTest/db/admin.sqlite`);
  if (fs.existsSync(target))fs.unlinkSync(target);
  fs.copyFileSync(path.normalize(`${__dirname}/loginTest/defaultDB/admin.sqlite`), target);
  const db = new Database(target);

  const target2 = path.normalize(`${__dirname}/loginTest/db/session.sqlite`);
  if (fs.existsSync(target2))fs.unlinkSync(target2);
  fs.copyFileSync(path.normalize(`${__dirname}/loginTest/defaultDB/session.sqlite`), target2);

  beforeEach(() => {
    db.exec('INSERT INTO persons (id, first_name, last_name, phone, email) VALUES (1, \'test\', \'\', NULL, \'test@example.com\');');
    db.exec('INSERT INTO users (id, name, person_id) VALUES (1, \'hello\', 1);');
    db.exec('INSERT INTO user_roles (user_id, role_id) VALUES (1, 2)');
    db.exec('INSERT INTO password_identifiers (id, identifier, password, reset_code, verified, user_id) VALUES (1, \'email:test@example.com\', \'#548d8bfe82fffe5d22748b7ba859a060415110de1eba43709c5fa26e7b1cd99370b5aecc264978053da7f98c581de9801e001c923c7479c34f9e9acb74b70caa\', NULL, 0, 1);');
  });

  afterEach(() => {
    db.exec('DELETE FROM persons');
    db.exec('DELETE FROM users');
    db.exec('DELETE FROM user_roles');
    db.exec('DELETE FROM password_identifiers');
  });

  test('test login page', async () => {
    const c = new ControllerAuth({ body: 'username=test&password=Hello1234!', headers: {}, query: {}, raw: { url: 'example.html' }, cookies: {} });
    const r = await c.execute('login');
    if (r.status === 500)console.log(c.error);
  });

  test('test login submit', async () => {
    const c = new ControllerAuth({ body: 'username=hello&password=Hello1234!', headers: {}, query: {}, raw: { url: 'example.html' }, cookies: {} });
    const r = await c.execute('auth');
    if (r.status === 500) console.log(c.error);
    expect(c.request.session.logged_in).toBe(true);

    const c2 = new ControllerAuth(c.request);
    const r2 = await c2.execute('logout');
    expect(c2.request.session.logged_in).toBe(false);
  });

  test('login fail', async () => {
    const c = new ControllerAuth({ body: 'username=hellox&password=Hello1234!', headers: {}, query: {}, raw: { url: 'example.html' }, cookies: {} });
    const r = await c.execute('auth');
    if (r.status === 500) {
      console.log(c.error);
    }
    expect(r.status).toBe(302);
    expect(c.error.message).toBe('user not found');
  });

  test('display login fail', async () => {
    const c = new ControllerAuth({ body: 'username=hellox&password=Hello1234!', headers: {}, query: {}, raw: { url: 'example.html' }, cookies: {} });
    const r = await c.execute('fail');
    expect(r.status).toBe(200);
  });

  test('forgot password', async () => {
    const c = new ControllerAuth({ body: '', headers: {}, query: {}, raw: { url: '' }, cookies: {} });
    const r = await c.execute('forgot_password');
    expect(r.status).toBe(200);
  });

  test('forgot password post', async () => {
    const c = new ControllerAuth({ body: 'username=hello', headers: {}, query: {}, raw: { url: '' }, cookies: {} });
    const r = await c.execute('forgot_password_post');
    if (r.status === 500)console.log(c.error);
    expect(r.status).toBe(302);

    const rs = db.prepare('SELECT * FROM password_identifiers').get();
    const resetCode = rs.reset_code;
    const sign = HelperRegistrar.resetPasswordSignature('hello', resetCode, { salt: KohanaJS.config.signup.forgotPassword.salt });

    const c2 = new ControllerAuth({ body: '', headers: {}, query: {}, raw: { url: '' }, cookies: {}, params: { sign, code: resetCode } });
    const r2 = await c2.execute('reset_password');
    expect(r2.status).toBe(200);

    const c2b = new ControllerAuth({ body: '', headers: {}, query: {}, raw: { url: '' }, cookies: {}, params: { sign, code: 'xxxxx' } });
    const r2b = await c2b.execute('reset_password');
    expect(r2b.status).toBe(500);
    expect(c2b.error.message).toBe('Invalid Password Reset Code or Reset code already used.');

    const c3 = new ControllerAuth({ body: 'username=hello&password=Hello1234!', headers: {}, query: {}, raw: { url: '' }, cookies: {}, params: { sign, code: resetCode } });
    const r3 = await c3.execute('reset_password_post');
    expect(r3.status).toBe(500);
    expect(c3.error.message).toBe('New password cannot same as previous password');

    const c3b = new ControllerAuth({ body: 'username=hello&password=Hello1234!', headers: {}, query: {}, raw: { url: '' }, cookies: {}, params: { sign, code: 'xxx' } });
    const r3b = await c3b.execute('reset_password_post');
    expect(r3b.status).toBe(500);
    expect(c3b.error.message).toBe('Invalid Password Reset Link');

    const c4 = new ControllerAuth({ body: 'username=hello&password=Hello1234!', headers: {}, query: {}, raw: { url: '' }, cookies: {}, params: { sign: 'xxx', code: resetCode } });
    const r4 = await c4.execute('reset_password_post');
    expect(r4.status).toBe(500);
    expect(c4.error.message).toBe('Invalid Password Reset Link');

    const c5 = new ControllerAuth({ body: 'username=hellp&password=Hello1234!', headers: {}, query: {}, raw: { url: '' }, cookies: {}, params: { sign, code: resetCode } });
    const r5 = await c5.execute('reset_password_post');
    expect(r5.status).toBe(500);
    expect(c5.error.message).toBe('Invalid Password Reset Link');

    const c6 = new ControllerAuth({ body: 'username=hello&password=Hello3333!', headers: {}, query: {}, raw: { url: '' }, cookies: {}, params: { sign, code: resetCode } });
    const r6 = await c6.execute('reset_password_post');
    if (r6.status === 500) console.log(c6.error);
    expect(r6.status).toBe(302);

    // login using new password
    const c7 = new ControllerAuth({ body: 'username=hello&password=Hello3333!', headers: {}, query: {}, raw: { url: 'example.html' }, cookies: {} });
    const r7 = await c7.execute('auth');
    if (r7.status === 500) console.log(c.error);
    expect(c7.request.session.logged_in).toBe(true);
  });

  test('forgot password post fail', async () => {
    const c = new ControllerAuth({ body: 'username=hello2', headers: {}, query: {}, raw: { url: '' }, cookies: {} });
    const r = await c.execute('forgot_password_post');
    expect(r.status).toBe(500);
    expect(c.error.message).toBe('user name not found');
  });

  test('forgot password result', async () => {
    const c = new ControllerAuth({ body: '', headers: {}, query: {}, raw: { url: '' }, cookies: {}, params: {} });
    const r = await c.execute('forgot_password_result');
    expect(r.status).toBe(200);
  });

  test('reset password result', async () => {
    const c = new ControllerAuth({ body: '', headers: {}, query: {}, raw: { url: '' }, cookies: {}, params: {} });
    const r = await c.execute('reset_password_result');
    expect(r.status).toBe(200);
  });

  test('forgot username', async () => {
    const c = new ControllerAuth({ body: '', headers: {}, query: {}, raw: { url: '' }, cookies: {} });
    const r = await c.execute('forgot_username');
    expect(r.status).toBe(200);
  });

  test('forgot username post', async () => {
    const c = new ControllerAuth({ body: 'email=test@example.com', headers: {}, query: {}, raw: { url: '' }, cookies: {} });
    const r = await c.execute('forgot_username_post');
    if (r.status === 500)console.log(c.error);
    expect(r.status).toBe(302);
  });

  test('forgot username result', async () => {
    const c = new ControllerAuth({ body: '', headers: {}, query: {}, raw: { url: '' }, cookies: {}, params: {} });
    const r = await c.execute('forgot_username_result');
    expect(r.status).toBe(200);
  });
});

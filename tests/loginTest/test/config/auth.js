const {KohanaJS} = require('kohanajs');

module.exports = {
  databasePath : KohanaJS.EXE_PATH + '/../db',
  salt: 'thisislonglonglonglongtextover32bytes'
}
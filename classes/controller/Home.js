const { Controller } = require('@kohanajs/core-mvc');
const { ControllerMixinMime, ControllerMixinView } = require('kohanajs');

class ControllerHome extends Controller.mixin([ControllerMixinMime, ControllerMixinView]) {
  async action_index() {
    this.setTemplate('templates/home');
  }
}

module.exports = ControllerHome;

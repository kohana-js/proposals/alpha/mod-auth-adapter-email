const { Controller } = require('@kohanajs/core-mvc');
const { ControllerMixinDatabase, ControllerMixinMime, ControllerMixinView, KohanaJS, ORM } = require('kohanajs');
const { ControllerMixinMultipartForm } = require('@kohanajs/mod-form');
const { DatabaseDriverBetterSQLite3 } = require('@kohanajs/mod-database');
const { ControllerMixinSession } = require('@kohanajs/mod-session');

const ControllerMixinLoginRequire = require('../controller-mixin/LoginRequire');// require('../controller-mixin/LoginRequire');
const ControllerMixinEmailAccount = require('../controller-mixin/EmailAccount');// require('../controller-mixin/EmailAccount')

const PasswordIdentifier = ORM.require('PasswordIdentifier');

class ControllerAccount extends Controller.mixin([
  ControllerMixinDatabase,
  ControllerMixinSession,
  ControllerMixinLoginRequire,
  ControllerMixinMultipartForm,
  ControllerMixinEmailAccount,
  ControllerMixinMime,
  ControllerMixinView,
]) {
  /**
   *
   * @param request
   * @param opts
   * @param opts.databaseMap
   * @param opts.allowRoles
   * @param opts.rejectLanding
   * @param opts.layout
   */
  constructor(request, opts = {}) {
    super(request);

    const {
      databaseMap = new Map([
        ['session', `${KohanaJS.config.auth.databasePath}/session.sqlite`],
        ['admin', `${KohanaJS.config.auth.databasePath}/admin.sqlite`],
      ]),
      driver = DatabaseDriverBetterSQLite3,
      allowRoles = ['admin'],
      rejectLanding = '/login',
      layout = 'layout/account',
      mailAdapter = KohanaJS.config.signup.mailAdapter,
    } = opts;

    this.state.get(ControllerMixinDatabase.DATABASE_MAP)
      .set('session', databaseMap.get('session'))
      .set('admin', databaseMap.get('admin'));
    this.state.set(ControllerMixinDatabase.DATABASE_DRIVER, driver);

    this.state.set(ControllerMixinView.LAYOUT_FILE, layout);
    this.state.set(ControllerMixinLoginRequire.REJECT_LANDING, rejectLanding);
    this.state.set(ControllerMixinLoginRequire.ALLOW_ROLES, new Set(allowRoles));
    this.state.set(ControllerMixinEmailAccount.MAIL_ADAPTER, mailAdapter);
  }

  async before() {
    this.state.get(ControllerMixinView.LAYOUT).data.user_role = this.request.session.user_role;
  }

  async action_index() {
    const user = this.state.get(ControllerMixinEmailAccount.INSTANCE);
    const person = await user.parent('person_id');
    const adminDB = this.state.get(ControllerMixinDatabase.DATABASES).get('admin');
    const identifier = await ORM.readBy(PasswordIdentifier, 'user_id', [user.id], { database: adminDB });

    this.setTemplate('templates/account/dashboard', {
      person,
      verified: !!identifier.verified,
    });
  }

  async action_activate() {
    const user = this.state.get(ControllerMixinEmailAccount.INSTANCE);
    this.setTemplate('templates/account/activate', { email: user.email });
  }

  async action_resend_verification() {
    this.setTemplate('templates/account/resend-verification');
  }

  async action_change_email() {
    const user = this.state.get(ControllerMixinEmailAccount.INSTANCE);
    const person = await user.parent('person_id');

    this.setTemplate('templates/account/change-email', { person });
  }

  async action_change_email_post() {
    const $_POST = this.state.get(ControllerMixinMultipartForm.POST_DATA);
    await this.redirect($_POST.destination || KohanaJS.config.signup.destination);
  }

  async action_change_password() {
    const user = this.state.get(ControllerMixinEmailAccount.INSTANCE);
    this.setTemplate('templates/account/change-password/form', { username: user.username });
  }

  async action_change_password_post() {
    const $_POST = this.state.get(ControllerMixinMultipartForm.POST_DATA);
    await this.redirect($_POST.destination || '/account/change-password-submit');
  }

  async action_change_password_result() {
    this.setTemplate('templates/account/change-password/submit');
  }
}

module.exports = ControllerAccount;

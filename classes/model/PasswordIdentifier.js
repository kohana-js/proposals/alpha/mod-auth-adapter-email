const {ORM} = require('kohanajs');

class PasswordIdentifier extends ORM{
  user_id = null;
  identifier = null;
  password = null;
  reset_code = "";
  verified = false;

  static joinTablePrefix = 'password_identifier';
  static tableName = 'password_identifiers';

  static fields = new Map([
    ["identifier", "String!"],
    ["password", "String!"],
    ["reset_code", "String"],
    ["verified", "Boolean"]
  ]);
  static belongsTo = new Map([
    ["user_id", "User"]
  ]);
}

module.exports = PasswordIdentifier;

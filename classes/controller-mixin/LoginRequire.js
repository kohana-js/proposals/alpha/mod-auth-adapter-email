const { ControllerMixin } = require('@kohanajs/core-mvc');
const { KohanaJS } = require('kohanajs');

class ControllerMixinLoginRequire extends ControllerMixin {
  static REJECT_LANDING = 'rejectLanding';

  static ALLOW_ROLES = 'allowRoles';

  static init(state) {
    state.set(this.REJECT_LANDING, state.get(this.REJECT_LANDING) || '/');
    state.set(this.ALLOW_ROLES, state.get(this.ALLOW_ROLES) || new Set(['admin']));
  }

  static async before(state) {
    const client = state.get(ControllerMixin.CLIENT);
    const { request } = client;
    const { session } = request;

    if (!session?.logged_in) {
      await client.redirect(`${state.get(this.REJECT_LANDING)}?cp=${encodeURIComponent(request.raw.url)}`);
      return;
    }

    const sessionRoles = session.roles;
    if (new Set(sessionRoles).has(KohanaJS.config.signup.rootUser)) return;

    const allowRoles = state.get(this.ALLOW_ROLES);
    const intersection = sessionRoles.filter(it => allowRoles.has(it));

    if (!intersection.length) {
      await client.redirect(`${state.get(this.REJECT_LANDING)}?cp=${encodeURIComponent(request.raw.url)}&exit=role_mismatch`);
    }
  }
}

module.exports = ControllerMixinLoginRequire;

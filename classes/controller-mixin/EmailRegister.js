const { randomUUID } = require('crypto');
const { ControllerMixin } = require('@kohanajs/core-mvc');
const { KohanaJS, ORM, ControllerMixinDatabase } = require('kohanajs');
const { ControllerMixinMultipartForm } = require('@kohanajs/mod-form');
const { MailAdapter } = require('@kohanajs/mod-mail');

const HelperRegistrar = require('../helper/Registrar');
const Auth = require('../Auth');

const User = ORM.require('User');

const PasswordIdentifier = ORM.require('PasswordIdentifier');

class ControllerMixinEmailRegister extends ControllerMixin {
  static MAIL_ADAPTER = 'mail_adapter';

  static HELPER_REGISTRAR = 'helper_registrar';

  static init(state) {
    state.set(this.MAIL_ADAPTER, MailAdapter);
  }

  static async setup(state) {
    const client = state.get(ControllerMixin.CLIENT);
    const { request, clientIP } = client;
    const { hostname } = request;
    const mailDB = state.get(ControllerMixinDatabase.DATABASES).get('mail');

    state.set(this.HELPER_REGISTRAR, state.get(this.HELPER_REGISTRAR) || new HelperRegistrar(hostname, clientIP, { mailDB, mailAdapter: state.get(this.MAIL_ADAPTER) }));
  }

  static async action_forgot_password_post(state) {
    const adminDB = state.get(ControllerMixinDatabase.DATABASES).get('admin');
    const { username } = state.get(ControllerMixinMultipartForm.POST_DATA);

    const user = await ORM.readBy(User, 'name', [username], { database: adminDB, limit: 1 });
    if (!user) throw new Error('user name not found');
    const person = await user.parent('person_id');
    const identifier = await ORM.readAll(PasswordIdentifier, { database: adminDB, limit: 1, kv: new Map([['user_id', user.id], ['identifier', `email:${person.email}`]]) });

    identifier.reset_code = randomUUID();
    await identifier.write();
    await state.get(this.HELPER_REGISTRAR).sendResetPassword(username, person.email, identifier.reset_code, {
      tokens: {
        first_name: person.first_name,
        last_name: person.last_name,
      } });
  }

  static async action_reset_password(state) {
    const client = state.get(ControllerMixin.CLIENT);
    const { request } = client;
    const { code } = request.params;
    const adminDB = state.get(ControllerMixinDatabase.DATABASES).get('admin');

    const identifier = await ORM.readBy(PasswordIdentifier, 'reset_code', [code], { database: adminDB, limit: 1 });

    if (!identifier) {
      throw new Error('Invalid Password Reset Code or Reset code already used.');
    }
  }

  static async action_reset_password_post(state) {
    const client = state.get(ControllerMixin.CLIENT);
    const { request } = client;

    const { sign, code } = request.params;
    const $_POST = state.get(ControllerMixinMultipartForm.POST_DATA);
    const { username, password } = $_POST;
    const retype = $_POST['password-retype'];

    if (retype && retype !== password) {
      throw new Error('Password and Retype Password does not match');
    }

    // verify
    if (HelperRegistrar.resetPasswordSignature(username, code) !== sign) {
      throw new Error('Invalid Password Reset Link');
    }
    const adminDB = state.get(ControllerMixinDatabase.DATABASES).get('admin');

    const user = await ORM.readBy(User, 'name', [username], { database: adminDB });
    const identifier = await ORM.readAll(PasswordIdentifier, { database: adminDB, limit: 1, kv: new Map([['reset_code', code], ['user_id', user.id]]) });

    // change the password hash
    const newPasswordHash = Auth.hashPassword(user.name, password, KohanaJS.config.auth.salt);
    if (newPasswordHash === identifier.password) {
      throw new Error('New password cannot same as previous password');
    }

    identifier.password = newPasswordHash;
    identifier.reset_code = '';
    await identifier.write();
  }

  static async action_forgot_username_post(state) {
    const { email } = state.get(ControllerMixinMultipartForm.POST_DATA);
    const adminDB = state.get(ControllerMixinDatabase.DATABASES).get('admin');

    const identifier = await ORM.readAll(PasswordIdentifier, { database: adminDB, limit: 1, kv: new Map([['identifier', `email:${email}`]]) });
    const user = await ORM.factory(User, identifier.user_id, { database: adminDB, limit: 1 });

    await state.get(this.HELPER_REGISTRAR).sendUsername(user.username, email);
  }
}

module.exports = ControllerMixinEmailRegister;

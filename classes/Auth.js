const { ORM } = require('kohanajs');

const crypto = require('crypto');

const User = ORM.require('User');
const PasswordIdentifier = ORM.require('PasswordIdentifier');
const Login = ORM.require('Login');

class Auth {
  /**
   *
   * @param username
   * @param password
   * @param database
   * @param salt
   * @param {object} opts
   * @param {string} opts.ip
   * @returns {Promise<{session: {user_role: *, user_id: *, role_id: number, logged_in: boolean, user_full_name: string}, user: (*[]|Object)}>}
   */
  static async authorize(username, password, database, salt, opts = {}) {
    const user = await ORM.readBy(User, 'name', [username], { database, limit: 1 });
    if (!user) throw new Error('user not found');

    const { ip = '0.0.0.0' } = opts;

    const hashPassword = Auth.hashPassword(username, password, salt);
    const identifiers = await user.children('user_id', PasswordIdentifier);

    let matched = false;
    identifiers.forEach(identifier => {
      if (identifier.password === hashPassword)matched = true;
    });
    if (!matched) throw new Error('user name and password mismatch');

    await user.eagerLoad({
      with: ['Person', 'Role']
    })

    const login = ORM.create(Login, { database });
    login.ip = ip;
    login.user_id = user.id;
    await login.write();

    return {
      user,
      session: {
        user_full_name: `${user.person.first_name} ${user.person.last_name}`,
        user_id: user.id,
        roles: user.roles.map(it => it.name),
        role_ids:user.roles.map(it => it.id),
        logged_in: true,
      },
    };
  }

  /**
   *
   * @param username
   * @param password
   * @param salt
   * @returns {string}
   */
  static hashPassword(username, password, salt) {
    // digest password hash.
    const hash = crypto.createHash('sha512');
    hash.update(username + password + salt);
    return `#${hash.digest('hex')}`;
  }

  static logout(session) {
    Object.assign(session, {
      logged_in: false,
      user_id: null,
      role_id: null,
      user_full_name: null,
      user_role: null,
    });
  }
}

module.exports = Auth;
